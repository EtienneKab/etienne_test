import fileinput,sys,os
import numpy as np
import math as mt
import rpy2.robjects as R0
r = R0.r



out="/mnt/lustre/groups/CBBI0818/SAX/CALL/ImpG/Genome/"
fout=out+"ALL.ps"
fin= open(fout,"wt")
fin.writelines("SNP"+"\t"+"CHR"+"\t"+"BP"+"\t"+"SAX"+"\t"+"SCP"+"\n")

rows = {}

for line in fileinput.input(out+"scz.impz"):
	data = line.split()
	if fileinput.lineno() > 1:
		rows[data[1]] = data[5]
tmp = set()
for line in fileinput.input(out+"sax.impz"):
        data = line.split()
        if fileinput.lineno() > 1:
 		if data[1] in rows:
			fin.write("\t".join([data[1],data[0],data[2],data[5],rows[data[1]]])+"\n")
			tmp.add(data[1])
rows = {}
all =set()
for line in fileinput.input(out+"bip.impz"):
        data = line.split()
        if fileinput.lineno() > 1:
		all.add(data[1])

tmp = set(tmp).intersection(all)
all=set()
for line in fileinput.input(out+"mdd.impz"):
        data = line.split()
        if fileinput.lineno() > 1:
                all.add(data[1])
tmp = set(tmp).intersection(all)
all=set()
for line in fileinput.input(out+"adhd.impz"):
        data = line.split()
        if fileinput.lineno() > 1:
                all.add(data[1])
tmp = set(tmp).intersection(all)

fi  = open(out+"COM.snp","wt")
for d in list(tmp):
	fi.writelines(d+"\n")
fi.close()
