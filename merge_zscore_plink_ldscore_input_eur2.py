import fileinput,sys,os
import numpy as np
import scipy.special as scsp
import math as mt
import scipy.stats as st
#import rpy2.robjects as R0
#r = R0.r

ANC="B" #sys.argv[1]

filein2="/mnt/lustre/groups/CBBI0818_2/SCD_GWAS/Sickle_CKD_GWAS/IMPG/GENOME/"+str(ANC)+"/"

out="/mnt/lustre/groups/CBBI0818_2/SCD_GWAS/Sickle_CKD_GWAS/IMPG/FINAL_ZSCORE_RESULTS/"+str(ANC)+"/"
#out1="/mnt/lustre/groups/CBBI0818/SAX/CALL/LDSCORE/"

fin=open(out+str(ANC)+"."+"impz","wt")	## plink format merged
#fin1=open(out+str(ANC)+".GC."+"impz","wt")  ## plink format merged
fi=open(out+str(ANC)+".results_new."+"impz","wt")  ### lDSCORE format merged

fin.write("\t".join(["CHR","SNP","BP","A1","A2","P","OR","SE","P1"])+"\n")

fi.write(" ".join(["CHR","SNP","BP","A1","A2","P","OR","SE","P1"])+"\n")

for sam in ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22"]:
	rows = {};MAP = {};rows1 = {}
	#BIM="/mnt/lustre/groups/CBBI0818/CPGG/PGC/LDSCORE/1000G_EUR_Phase3_plink/1000G.EUR.QC."+str(sam)+".bim"
	#for line in fileinput.input(BIM):
	#		data = line.split()
	#	MAP[data[1]]=data[3]
	#FRQ="/mnt/lustre/groups/1000genomes/annotation/ImPG_REF/1000_Genomes.phase1_release_v3/1000G.EUR.QC."+str(sam)+".frq"
	#for line in fileinput.input(FRQ):
	#	data = line.split()
	#	if fileinput.lineno()>1:
	#		if data[1] in MAP:
	#			pass #rows[MAP[data[1]]]=[float(data[4])]+[data[1],MAP[data[1]],data[2],data[3]]
	#			#rows1[data[1]] = [float(data[4])]+[data[1],MAP[data[1]],data[2],data[3]]
					
	MAP = {}
	print(filein2+str(ANC)+"."+str(sam)+".impz")
	for line in fileinput.input(filein2+str(ANC)+"."+str(sam)+".impz"):
		if fileinput.lineno()>1:
			data = line.split()
			p = st.norm.pdf(abs(float(data[4])))  #2*(r.pnorm(-abs(float(data[4])))[0])
			p1 = 0.5*(1 + scsp.erf(float(data[4]) / np.sqrt(2)))
			SE_pseudo = mt.sqrt(float(data[5]))
			OR = float(data[4])*SE_pseudo
			n = 0
			SE = mt.exp(mt.sqrt(float(data[5])))

			if p in [1.0,0.0,0.9]:
				pass
			elif OR in [0.0,1.0,'-inf','inf']:
				pass
			else:
				if p <= 0.0009:
					fi.write("\t".join([str(sam),data[0],data[1],data[2],data[3],str(p),str(OR),str(SE_pseudo),str(p1)])+"\n")
				fin.write("\t".join([str(sam),data[0],data[1],data[2],data[3],str(p),str(OR),str(SE_pseudo),str(p1)])+"\n")
			#if data[1] in rows: 
			#	frq = str(rows[data[1]][0])
			#	fi.write(" ".join([rows[data[1]][1], str(sam),rows[data[1]][2],rows[data[1]][3],rows[data[1]][4],str(OR),str(SE),str(p),str(0.85),str(n),str(frq)])+"\n")
			#elif data[0] in rows1:
			#	frq = str(rows1[data[0]][0])
			#	fi.write(" ".join([rows1[data[0]][1], str(sam),rows1[data[0]][2],rows1[data[0]][3],rows1[data[0]][4],str(OR),str(SE),str(p),str(0.85),str(n),str(frq)])+"\n")
			#else:
			#	pass
		else:
			pass

fin.close();fi.close()

