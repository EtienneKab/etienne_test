import fileinput,sys,os
import numpy as np
import math as mt
import rpy2.robjects as R0
r = R0.r

ANC=sys.argv[1]

filein2="/mnt/lustre/groups/CBBI0818/CPGG/PGC/IMPG/FINAL_ZSCORE_RESULTS/"+str(ANC)+"/"
#out="/mnt/lustre/groups/CBBI0818/SAX/CALL/ImpG/Genome/"
out1="/mnt/lustre/groups/CBBI0818/SAX/CALL/LDSCORE/"+str(ANC)+"/"

COM = {}

for line in fileinput.input("/mnt/lustre/groups/CBBI0818/SAX/CALL/ImpG/Genome/COM.snp"):
	data = line.split()
	COM[data[0]]="i"

fi=open(out1+str(ANC)+".impz","wt")
fi.write(" ".join(["MarkerName","Allele1","Allele2","Freq.Allele1","p","N"])+"\n")

for sam in ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22"]:
	rows = {};MAP = {};rows1 = {}
	#fi=open(out1+str(ANC)+"."+str(sam)+".impz","wt")
	#fi.write(" ".join(["snpid","hg19chr","bp","a1","a2","or","se","pval","info","ngt","euraf"])+"\n")
	#fi.write(" ".join(["MarkerName","Allele1","Allele2","Freq.Allele1","p","N"])+"\n")
	BIM="/mnt/lustre/groups/CBBI0818/CPGG/PGC/LDSCORE/1000G_EUR_Phase3_plink/1000G.EUR.QC."+str(sam)+".bim"
	for line in fileinput.input(BIM):
		data = line.split()
		if data[1] in COM:
			MAP[data[1]]=data[3]
	FRQ="/mnt/lustre/groups/CBBI0818/CPGG/PGC/LDSCORE/1000G_Phase3_frq/1000G.EUR.QC."+str(sam)+".frq"
	for line in fileinput.input(FRQ):
		data = line.split()
		if fileinput.lineno()>1:
			if data[1] in MAP:
				rows[MAP[data[1]]]=[float(data[4])]+[data[1],MAP[data[1]],data[2],data[3]]
				rows1[data[1]] = [float(data[4])]+[data[1],MAP[data[1]],data[2],data[3]]
					
	MAP = {}
	print(filein2+str(ANC)+"."+str(sam)+".impz")
	for line in fileinput.input(filein2+str(ANC)+"."+str(sam)+".impz"):
		if fileinput.lineno()>1:
			data = line.split()
			p = 2*(r.pnorm(-abs(float(data[4])))[0])
			#SE_pseudo = mt.sqrt(float(data[5]))
			#OR = mt.exp(float(data[4])*SE_pseudo)
			if str(ANC)=="scz":
				n=150064
			elif str(ANC)=="bip":
				n=16731
			elif str(ANC)=="mdd":
				n=18759
			elif str(ANC)=="adhd":
				n=5422
			else:
				n=400
			#SE = mt.exp(mt.sqrt(float(data[5])))
			#fin.write("\t".join([str(sam),data[0],data[1],data[2],data[3],data[4],str(p),str(OR)])+"\n")
			if data[1] in rows: 
				frq = str(rows[data[1]][0])
				#fi.write(" ".join([rows[data[1]][1], str(sam),rows[data[1]][2],rows[data[1]][3],rows[data[1]][4],str(OR),str(SE),str(p),str(0.85),str(n),str(frq)])+"\n")
				fi.write(" ".join([rows[data[1]][1],rows[data[1]][3],rows[data[1]][4],str(frq),str(p),str(n)])+"\n")				
			elif data[0] in rows1:
				frq = str(rows1[data[0]][0])
				#fi.write(" ".join([rows1[data[0]][1], str(sam),rows1[data[0]][2],rows1[data[0]][3],rows1[data[0]][4],str(OR),str(SE),str(p),str(0.85),str(n),str(frq)])+"\n")
				fi.write(" ".join([rows1[data[0]][1], rows1[data[0]][3],rows1[data[0]][4],str(frq),str(p),str(n)])+"\n")
			else:
				pass
		else:
			pass

fi.close();#fin.close()
