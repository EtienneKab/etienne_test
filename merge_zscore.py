import fileinput,sys,os
import numpy as np
import rpy2.robjects as R0
r = R0.r

path=sys.argv[1]
ANC=sys.argv[2]

fin=open("/mnt/lustre/groups/CBBI0818/SAC/IMPG/Genome/ImpG_GWAS."+ANC,"wt")
rows = {}
for CHR in range(1, 23):
	for line in fileinput.input(path+ANC+"."+str(CHR)+".impz"):
		if fileinput.lineno()>1:
			#p = 2*(r.pnorm(abs(float(data[-2])))[0])
			rows[data[0]]=float(data[-2])
			#fin.write("\t".join(data+[str(p)])+"\n")
		else:
			if CHR==1:
				fin.write("\t".join(data+["Pvalues","GC"])+"\n"))
			else:
				pass

z=rows.values()
M = r.median(z,na.rm=TRUE)
QC = np.max(1,M)/0.455
for CHR in range(1, 23):
        for line in fileinput.input(path+ANC+"."+str(CHR)+".impz"):
                if fileinput.lineno()>1:
                        p = 2*(r.pnorm(abs(float(data[-2])))[0])
                        z = float(data[-2])/QC
			p1 = np.exp(-0.73*z-0.416*(z**2))
			rows[data[0]]=float(data[-2])
                        fin.write("\t".join(data+[str(p),str(p1)])+"\n")
fin.close()
