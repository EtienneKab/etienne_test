import fileinput,sys,os
import numpy as np
import math as mt
import rpy2.robjects as R0
r = R0.r

ANC=sys.argv[1]

filein2="/mnt/lustre/groups/CBBI0818/DELESA/projects/malariagen/analysis_ec/IMPG/GENOME/"+ANC+"/"
out1="/mnt/lustre/groups/CBBI0818/DELESA/projects/malariagen/analysis_ec/LDSCORE/"


#fin=open(out+str(ANC)+"."+"impz","wt")	## plink format merged
#fin.write("\t".join(["CHR","SNP","BP","A1","A2","P","OR"])+"\n")

fi=open(out1+str(ANC)+".impz","wt")
fi.write(" ".join(["MarkerName","Allele1","Allele2","Freq.Allele1","p","N"])+"\n")

for sam in ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22"]:
	rows = {};MAP = {};rows1 = {}
	BIM="/mnt/lustre/groups/CBBI0818/CPGG/PGC_new/LDSCORE/1000G_EUR_Phase3_plink/1000G.EUR.QC."+str(sam)+".bim"
	for line in fileinput.input(BIM):
		data = line.split()
		#if data[1] in COM:
		MAP[data[1]]=data[3]
	FRQ="/mnt/lustre/groups/CBBI0818/CPGG/PGC_new/LDSCORE/1000G_Phase3_frq/1000G.EUR.QC."+str(sam)+".frq"
	for line in fileinput.input(FRQ):
		data = line.split()
		if fileinput.lineno()>1:
			if data[1] in MAP:
				rows[MAP[data[1]]]=[float(data[4])]+[data[1],MAP[data[1]],data[2],data[3]]
				rows1[data[1]] = [float(data[4])]+[data[1],MAP[data[1]],data[2],data[3]]
					
	MAP = {}
	print(filein2+str(ANC)+"."+str(sam)+".impz")
	for line in fileinput.input(filein2+str(ANC)+"."+str(sam)+".impz"):
		if fileinput.lineno()>1:
			data = line.split()
			p = 2*(r.pnorm(-abs(float(data[4])))[0])
			SE_pseudo = mt.sqrt(float(data[5]))
			OR = mt.exp(float(data[4])*SE_pseudo)
			if ANC =="Gambia":
                                n=4920
                        elif ANC=="Malawi":
                                n=2516
                        elif ANC=="Kenya":
                                n=3142
                        elif ANC=="Meta":
                                n=4920+2516+3142
                        elif ANC=="MERGE":
                                n=4920+2516+3142
			SE = mt.exp(mt.sqrt(float(data[5])))
			#fin.write("\t".join([str(sam),data[0],data[1],data[2],data[3],str(p),str(OR)])+"\n")
			if data[1] in rows: 
				frq = str(rows[data[1]][0])
				#fi.write(" ".join([rows[data[1]][1], str(sam),rows[data[1]][2],rows[data[1]][3],rows[data[1]][4],str(OR),str(SE),str(p),str(0.85),str(n),str(frq)])+"\n")
				fi.write(" ".join([rows[data[1]][1],rows[data[1]][3],rows[data[1]][4],str(frq),str(p),str(n)])+"\n")				
			elif data[0] in rows1:
				frq = str(rows1[data[0]][0])
				#fi.write(" ".join([rows1[data[0]][1], str(sam),rows1[data[0]][2],rows1[data[0]][3],rows1[data[0]][4],str(OR),str(SE),str(p),str(0.85),str(n),str(frq)])+"\n")
				fi.write(" ".join([rows1[data[0]][1], rows1[data[0]][3],rows1[data[0]][4],str(frq),str(p),str(n)])+"\n")
			else:
				pass
		else:
			pass

fi.close();#fin.close()
